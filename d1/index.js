// SECTION - JS Synchronous cs Asynchronous
// JavaScript is by default synchronouse, meaning that only one statement will be executed at a time.
// This can be proven when a statement has an error, JavaScript will not proceed with the next statement
/*console.log(`Hello World`);
consle.log(`Hello Again`);
console.log(`Goodbye`);
*/

// When statments take time to process, this slows down our code.
// An example of this when loops are used on a large amount of information or wehn fetching data from databases
// We might not notice it due to the fast processing power of our devices
// This is the reason some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed

/*for (let i =1; i <= 1500; i ++){
	console.log(i);
};

console.log("it's me again");*/

// Fetch API - allows ys to asynchronously request for a resource (data);
// Asynchronous - allows executing of codes simultaneously prioritizing on the less resource - consuming codes while the more complext and more resource-consuming codes run at the back
// A "Promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
/*
	SYNTAX:
		fetch(URL);
*/
//console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// retrieves all posts following the REST API (retrieve, /posts, GET)
fetch("https://jsonplaceholder.typicode.com/posts")
// by using the ".then" mmethod, we can now check the status of the promise
// "fetch" method will return a "promise" that resolves to a response object
// the ".hten" method captures the response object and returns another "promise" which will eventually be resolved or rejected.
//.then(response => console.log(response.status))

// we use the "json" method from the response object to convert the data retrieved into JSON format to be used in our application
.then(response => response.json())

// using multiple ".then" methods will result into "promise chain"
// the we can now access it and log in the console our desired output.
.then(json => console.log(json))

console.log("Hello");

// ASYNC-AWAIT
// "async" and "await" keywords are other approached that can be used to perform asynchronous javascript
// used in functions to indicate which portions of the code should be waited for
async function fetchData () {
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json)

	console.log
};

fetchData();

console.log("Hello");

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));



// SECTION - updating a post (PUT)
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// SECTION - updating a post (PATCH)
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post",

	})
})
.then(response => response.json())
.then(json => console.log(json));

// SECTION - deleting of a resource
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});

// SECTION - Filter post
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(response => response.json())
.then(json => console.log(json));

// Retrieving comments for a specific post. accessing nested.embedded comments
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response => response.json())
.then(json => console.log(json));
