// GET fetch
fetch( "https://jsonplaceholder.typicode.com/todos", {
		method: "GET",
		headers: {"Content-Type": "application/json"},
	} )
	.then(response => response.json())
	.then(json => console.log(json));

// GET fetch
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "GET",
		headers: {"Content-Type": "application/json"},
})
	.then(response => response.json())
	.then(json => console.log(json));

// POST fetch
fetch( "https://jsonplaceholder.typicode.com/todos", {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		})
	.then(response => response.json())
	.then(json => console.log(json));

// PUT fetch
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PUT",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			title:"Title",
			description: "Description",
			status: "Status",
			dateCompleted: "",
			userId: 1
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));

// PATCH
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "PATCH",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({
			dateCompleted: "July 27, 2022"
		})
	} )
	.then(response => response.json())
	.then(json => console.log(json));

// DETELE
fetch( "https://jsonplaceholder.typicode.com/todos/1", {
		method: "DELETE"
	} )